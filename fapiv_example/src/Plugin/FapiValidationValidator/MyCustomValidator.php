<?php

namespace Drupal\fapiv_example\Plugin\FapiValidationValidator;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\fapi_validation\Attribute\FapiValidationValidator;
use Drupal\fapi_validation\FapiValidationValidatorsInterface;
use Drupal\fapi_validation\Validator;

/**
 * Provides a custom validation.
 */
#[FapiValidationValidator(
  id: 'custom_validator',
  label: new TranslatableMarkup('Custom Validator'),
  description: new TranslatableMarkup('Field must have JohnDoe as value.'),
  error_callback: 'processError',
)]
class MyCustomValidator implements FapiValidationValidatorsInterface {

  /**
   * {@inheritdoc}
   */
  public function validate(Validator $validator, array $element, FormStateInterface $form_state) {
    return $validator->getValue() == 'JohnDoe';
  }

  /**
   * Process custom error.
   *
   * @param Drupal\fapi_validation\Validator $validator
   *   Validator.
   * @param array $element
   *   Form element.
   *
   * @return string
   *   Error message.
   */
  public static function processError(Validator $validator, array $element) {
    $params = [
      '%value' => $validator->getValue(),
      '%field' => $element['#title'],
    ];
    return \t("You must enter 'JohnDoe' as value and not '%value' at field %field", $params);
  }

}
