<?php

namespace Drupal\fapiv_example\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements the SimpleForm form controller.
 *
 * This example demonstrates a simple form with a singe text input element. We
 * extend FormBase which is the simplest form base class used in Drupal.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class SimpleForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fapiv_example_simple_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#description' => $this->t('Title must be at least 5 characters in length.'),
      '#validators' => ['rule' => 'length[5, *]'],
      '#filters' => ['uppercase', 'trim'],
      '#required' => TRUE,
    ];

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#description' => $this->t('The Value should be JohnDoe.'),
      '#validators' => [
        ['rule' => 'length[7]', 'error' => 'Wrong name size of field %field.'],
        'custom_validator',
      ],
      '#required' => TRUE,
    ];

    $form['mail'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email'),
      '#description' => $this->t('Email must be valid e-mail address.'),
      '#validators' => ['email'],
      '#required' => TRUE,
    ];

    $form['range'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Range'),
      '#description' => $this->t('The value should be between 0 and 100.'),
      '#validators' => ['rule' => 'range[0, 100]'],
      '#required' => TRUE,
    ];

    $form['ip'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IP address'),
      '#description' => $this->t('The value should be valid IP address. e.g. 127.0.0.1'),
      '#validators' => ['ipv4'],
      '#required' => TRUE,
    ];

    $form['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL'),
      '#description' => $this->t('The value should be valid absolute URL. e.g. http://example.com'),
      '#validators' => ['url[absolute]'],
      '#required' => TRUE,
    ];

    // Group submit handlers in an actions element with a key of "actions" so
    // that it gets styled correctly, and so that other modules may add actions
    // to the form. This is not required, but is convention.
    $form['actions'] = [
      '#type' => 'actions',
    ];

    // Add a submit button that handles the submission of the form.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $title = $form_state->getValue('title');
    $this->messenger()->addMessage($this->t('You specified a title of %title.', ['%title' => $title]));
  }

}
