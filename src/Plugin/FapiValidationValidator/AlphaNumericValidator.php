<?php

namespace Drupal\fapi_validation\Plugin\FapiValidationValidator;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\fapi_validation\Attribute\FapiValidationValidator;
use Drupal\fapi_validation\FapiValidationValidatorsInterface;
use Drupal\fapi_validation\Validator;

/**
 * Fapi Validation Plugin for Alpha Numeric validation.
 */
#[FapiValidationValidator(
  id: 'alpha_numeric',
  label: new TranslatableMarkup('Alpha Numeric'),
  description: new TranslatableMarkup('Validates input value on the correct alpha numeric pattern.'),
  error_message: 'Use only alpha numerics characters at %field.',
)]
class AlphaNumericValidator implements FapiValidationValidatorsInterface {

  /**
   * {@inheritdoc}
   */
  public function validate(Validator $validator, array $element, FormStateInterface $form_state) {
    return (bool) preg_match('/^[\pL]++$/uD', (string) $validator->getValue());
  }

}
