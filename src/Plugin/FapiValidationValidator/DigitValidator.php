<?php

namespace Drupal\fapi_validation\Plugin\FapiValidationValidator;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\fapi_validation\Attribute\FapiValidationValidator;
use Drupal\fapi_validation\FapiValidationValidatorsInterface;
use Drupal\fapi_validation\Validator;

/**
 * Fapi Validation Plugin for Digit validation.
 */
#[FapiValidationValidator(
  id: 'digit',
  label: new TranslatableMarkup('Digit'),
  description: new TranslatableMarkup('Validates input value based on digit pattern.'),
  error_message: 'Use only digits on %field.',
)]
class DigitValidator implements FapiValidationValidatorsInterface {

  /**
   * {@inheritdoc}
   */
  public function validate(Validator $validator, array $element, FormStateInterface $form_state) {
    return (bool) preg_match('/^\d+$/', (string) $validator->getValue());
  }

}
