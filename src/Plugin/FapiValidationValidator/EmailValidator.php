<?php

namespace Drupal\fapi_validation\Plugin\FapiValidationValidator;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\fapi_validation\Attribute\FapiValidationValidator;
use Drupal\fapi_validation\FapiValidationValidatorsInterface;
use Drupal\fapi_validation\Validator;

/**
 * Fapi Validation Plugin for Email validation.
 */
#[FapiValidationValidator(
  id: 'email',
  label: new TranslatableMarkup('Email'),
  description: new TranslatableMarkup('Validates an email address.'),
  error_message: '%field is not a valid email.',
)]
class EmailValidator implements FapiValidationValidatorsInterface {

  /**
   * {@inheritdoc}
   */
  public function validate(Validator $validator, array $element, FormStateInterface $form_state) {
    return fapi_validation_validate_email($validator->getValue());
  }

}
