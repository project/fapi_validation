<?php

namespace Drupal\fapi_validation\Plugin\FapiValidationValidator;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\fapi_validation\Attribute\FapiValidationValidator;
use Drupal\fapi_validation\FapiValidationValidatorsInterface;
use Drupal\fapi_validation\Validator;

/**
 * Fapi Validation Plugin for IPV4 validation.
 */
#[FapiValidationValidator(
  id: 'ipv4',
  label: new TranslatableMarkup('IPV4'),
  description: new TranslatableMarkup('Validates input value on correct ipv4 pattern.'),
  error_message: 'Invalid format of %field.',
)]
class Ipv4Validator implements FapiValidationValidatorsInterface {

  /**
   * {@inheritdoc}
   */
  public function validate(Validator $validator, array $element, FormStateInterface $form_state) {
    $pattern = '/^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])'
      . '(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$/';
    return (bool) preg_match($pattern, $validator->getValue());
  }

}
