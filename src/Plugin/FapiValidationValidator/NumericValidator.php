<?php

namespace Drupal\fapi_validation\Plugin\FapiValidationValidator;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\fapi_validation\Attribute\FapiValidationValidator;
use Drupal\fapi_validation\FapiValidationValidatorsInterface;
use Drupal\fapi_validation\Validator;

/**
 * Fapi Validation Plugin for Numeric validation.
 */
#[FapiValidationValidator(
  id: 'numeric',
  label: new TranslatableMarkup('Numeric'),
  description: new TranslatableMarkup('Finds whether a variable is a number or a numeric string.'),
  error_message: 'Use only numbers at %field.',
)]
class NumericValidator implements FapiValidationValidatorsInterface {

  /**
   * {@inheritdoc}
   */
  public function validate(Validator $validator, array $element, FormStateInterface $form_state) {
    return is_numeric($validator->getValue());
  }

}
