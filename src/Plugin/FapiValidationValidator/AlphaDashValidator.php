<?php

namespace Drupal\fapi_validation\Plugin\FapiValidationValidator;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\fapi_validation\Attribute\FapiValidationValidator;
use Drupal\fapi_validation\FapiValidationValidatorsInterface;
use Drupal\fapi_validation\Validator;

/**
 * Fapi Validation Plugin for Alpha Dash validation.
 */
#[FapiValidationValidator(
  id: 'alpha_dash',
  label: new TranslatableMarkup('Alpha Dash'),
  description: new TranslatableMarkup('Validates input value based on alpha_dash pattern.'),
  error_message: 'Use only alpha numerics, hyphen and underscore at %field.',
)]
class AlphaDashValidator implements FapiValidationValidatorsInterface {

  /**
   * {@inheritdoc}
   */
  public function validate(Validator $validator, array $element, FormStateInterface $form_state) {
    return (bool) preg_match('/^[-\pL\pN_]+$/uD', (string) $validator->getValue());
  }

}
