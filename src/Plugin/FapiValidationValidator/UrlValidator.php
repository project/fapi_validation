<?php

namespace Drupal\fapi_validation\Plugin\FapiValidationValidator;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\fapi_validation\Attribute\FapiValidationValidator;
use Drupal\fapi_validation\FapiValidationValidatorsInterface;
use Drupal\fapi_validation\Validator;

/**
 * Fapi Validation Plugin for URL validation.
 */
#[FapiValidationValidator(
  id: 'url',
  label: new TranslatableMarkup('URL'),
  description: new TranslatableMarkup('Validates an URL address.'),
  error_message: 'Invalid format of %field.',
)]
class UrlValidator implements FapiValidationValidatorsInterface {

  /**
   * {@inheritdoc}
   */
  public function validate(Validator $validator, array $element, FormStateInterface $form_state) {
    $params = $validator->getParams();
    return UrlHelper::isValid($validator->getValue(), !empty($params) && $params[0] == 'absolute');
  }

}
