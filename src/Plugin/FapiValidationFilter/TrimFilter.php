<?php

namespace Drupal\fapi_validation\Plugin\FapiValidationFilter;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\fapi_validation\Attribute\FapiValidationFilter;
use Drupal\fapi_validation\FapiValidationFiltersInterface;

/**
 * Fapi Validation Plugin for Trim filter.
 */
#[FapiValidationFilter(
  id: 'trim',
  label: new TranslatableMarkup('Trim'),
  description: new TranslatableMarkup('Remove leading and trailing spaces.'),
)]
class TrimFilter implements FapiValidationFiltersInterface {

  /**
   * {@inheritdoc}
   */
  public function filter($value) {
    return trim($value);
  }

}
