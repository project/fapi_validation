<?php

namespace Drupal\fapi_validation\Plugin\FapiValidationFilter;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\fapi_validation\Attribute\FapiValidationFilter;
use Drupal\fapi_validation\FapiValidationFiltersInterface;

/**
 * Fapi Validation Plugin for Ucwords filter.
 */
#[FapiValidationFilter(
  id: 'ucwords',
  label: new TranslatableMarkup('Ucwords'),
  description: new TranslatableMarkup('Uppercase the first character of each word in a string.'),
)]
class UcwordsFilter implements FapiValidationFiltersInterface {

  /**
   * {@inheritdoc}
   */
  public function filter($value) {
    return function_exists(' mb_convert_case') ? mb_convert_case($value, MB_CASE_TITLE) : ucwords($value);
  }

}
