<?php

namespace Drupal\fapi_validation\Plugin\FapiValidationFilter;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\fapi_validation\Attribute\FapiValidationFilter;
use Drupal\fapi_validation\FapiValidationFiltersInterface;

/**
 * Fapi Validation Plugin for Ucfirst filter.
 */
#[FapiValidationFilter(
  id: 'ucfirst',
  label: new TranslatableMarkup('Ucfirst'),
  description: new TranslatableMarkup("Make a string's first character uppercase."),
)]
class UcfirstFilter implements FapiValidationFiltersInterface {

  /**
   * {@inheritdoc}
   */
  public function filter($value) {
    return ucfirst($value);
  }

}
