<?php

namespace Drupal\fapi_validation\Plugin\FapiValidationFilter;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\fapi_validation\Attribute\FapiValidationFilter;
use Drupal\fapi_validation\FapiValidationFiltersInterface;

/**
 * Fapi Validation Plugin for Rtrim filter.
 */
#[FapiValidationFilter(
  id: 'rtrim',
  label: new TranslatableMarkup('Rtrim'),
  description: new TranslatableMarkup('Strip whitespace (or other characters) from the end of a string.'),
)]
class RtrimFilter implements FapiValidationFiltersInterface {

  /**
   * {@inheritdoc}
   */
  public function filter($value) {
    return rtrim($value);
  }

}
