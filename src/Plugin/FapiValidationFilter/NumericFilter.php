<?php

namespace Drupal\fapi_validation\Plugin\FapiValidationFilter;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\fapi_validation\Attribute\FapiValidationFilter;
use Drupal\fapi_validation\FapiValidationFiltersInterface;

/**
 * Fapi Validation Plugin for remove Numeric filter.
 */
#[FapiValidationFilter(
  id: 'numeric',
  label: new TranslatableMarkup('Numeric'),
  description: new TranslatableMarkup('Remove Numeric filter from a string.'),
)]
class NumericFilter implements FapiValidationFiltersInterface {

  /**
   * {@inheritdoc}
   */
  public function filter($value) {
    return preg_replace('/[^0-9]+/', '', $value);
  }

}
