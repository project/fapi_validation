<?php

namespace Drupal\fapi_validation\Plugin\FapiValidationFilter;

use Drupal\Component\Transliteration\TransliterationInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\fapi_validation\Attribute\FapiValidationFilter;
use Drupal\fapi_validation\FapiValidationFiltersInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Fapi Validation Plugin for Machine name filter.
 */
#[FapiValidationFilter(
  id: 'machine_name',
  label: new TranslatableMarkup('Machine name'),
  description: new TranslatableMarkup('Transform input values into machine-readable names.'),
)]
class MachineNameFilter implements FapiValidationFiltersInterface, ContainerFactoryPluginInterface {

  /**
   * Constructs a MachineNameFilter object.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    array $plugin_definition,
    protected TransliterationInterface $transliteration
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('transliteration')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function filter($value) {
    $transliterated = $this->transliteration->transliterate($value, 'en', '_');
    $transliterated = mb_strtolower($transliterated);

    return preg_replace('@' . strtr('[^a-z0-9_]+', ['@' => '\@', chr(0) => '']) . '@', '_', $transliterated);
  }

}
