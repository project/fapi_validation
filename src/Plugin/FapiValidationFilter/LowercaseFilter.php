<?php

namespace Drupal\fapi_validation\Plugin\FapiValidationFilter;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\fapi_validation\Attribute\FapiValidationFilter;
use Drupal\fapi_validation\FapiValidationFiltersInterface;

/**
 * Fapi Validation Plugin for Lowercase filter.
 */
#[FapiValidationFilter(
  id: 'lowercase',
  label: new TranslatableMarkup('Lowercase'),
  description: new TranslatableMarkup('Transform input value to lower case.'),
)]
class LowercaseFilter implements FapiValidationFiltersInterface {

  /**
   * {@inheritdoc}
   */
  public function filter($value) {
    return function_exists('mb_strtolower') ? mb_strtolower($value) : strtolower($value);
  }

}
