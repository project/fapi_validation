<?php

namespace Drupal\fapi_validation\Plugin\FapiValidationFilter;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\fapi_validation\Attribute\FapiValidationFilter;
use Drupal\fapi_validation\FapiValidationFiltersInterface;

/**
 * Fapi Validation Plugin for Strip HTML tags filter.
 */
#[FapiValidationFilter(
  id: 'strip_tags',
  label: new TranslatableMarkup('Strip HTML tags'),
  description: new TranslatableMarkup('Strip HTML tags from a string.'),
)]
class StripTagsFilter implements FapiValidationFiltersInterface {

  /**
   * {@inheritdoc}
   */
  public function filter($value) {
    return strip_tags($value);
  }

}
