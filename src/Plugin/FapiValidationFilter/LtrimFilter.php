<?php

namespace Drupal\fapi_validation\Plugin\FapiValidationFilter;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\fapi_validation\Attribute\FapiValidationFilter;
use Drupal\fapi_validation\FapiValidationFiltersInterface;

/**
 * Fapi Validation Plugin for Ltrim filter.
 */
#[FapiValidationFilter(
  id: 'ltrim',
  label: new TranslatableMarkup('Ltrim'),
  description: new TranslatableMarkup('Strip whitespace (or other characters) from the beginning of a string.'),
)]
class LtrimFilter implements FapiValidationFiltersInterface {

  /**
   * {@inheritdoc}
   */
  public function filter($value) {
    return ltrim($value);
  }

}
