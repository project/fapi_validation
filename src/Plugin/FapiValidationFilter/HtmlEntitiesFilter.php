<?php

namespace Drupal\fapi_validation\Plugin\FapiValidationFilter;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\fapi_validation\Attribute\FapiValidationFilter;
use Drupal\fapi_validation\FapiValidationFiltersInterface;

/**
 * Fapi Validation Plugin for HTML Entities filter.
 */
#[FapiValidationFilter(
  id: 'html_entities',
  label: new TranslatableMarkup('HTML Entities'),
  description: new TranslatableMarkup('Convert all applicable characters to HTML entities.'),
)]
class HtmlEntitiesFilter implements FapiValidationFiltersInterface {

  /**
   * {@inheritdoc}
   */
  public function filter($value) {
    return htmlentities(html_entity_decode($value));
  }

}
