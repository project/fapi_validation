<?php

namespace Drupal\fapi_validation\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\fapi_validation\FapiValidationFiltersManager;
use Drupal\fapi_validation\FapiValidationValidatorsManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns a table with all fapi validation plugins.
 */
class FapiValidationController extends ControllerBase {

  /**
   * Returns the plugin.manager.fapi_validation_validators service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $validationManager;

  /**
   * Returns the plugin.manager.fapi_validation_filters service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $filterManager;

  /**
   * Constructs a FapiValidationController object.
   *
   * @param \Drupal\fapi_validation\FapiValidationValidatorsManager $validation_manager
   *   *   A plugin manager for Fapi Validation Validators Plugin.
   * @param \Drupal\fapi_validation\FapiValidationFiltersManager $filter_manager
   *   A plugin manager for Fapi Validation Filters Plugin.
   */
  public function __construct(FapiValidationValidatorsManager $validation_manager, FapiValidationFiltersManager $filter_manager) {
    $this->validationManager = $validation_manager;
    $this->filterManager = $filter_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.fapi_validation_validators'),
      $container->get('plugin.manager.fapi_validation_filters')
    );
  }

  /**
   * Show all validation plugins.
   */
  public function validations() {
    $header = [
      ['data' => $this->t('ID')],
      ['data' => $this->t('Label')],
      ['data' => $this->t('Description')],
      ['data' => $this->t('Provider')],
    ];

    $rows = [];
    foreach ($this->validationManager->getDefinitions() as $validation) {
      $rows[] = [
        $validation['id'],
        $validation['label'] ?? '',
        $validation['description'] ?? '',
        $validation['provider'],
      ];
    }

    return [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];
  }

  /**
   * Show all filter plugins.
   */
  public function filters() {
    $header = [
      ['data' => $this->t('ID')],
      ['data' => $this->t('Label')],
      ['data' => $this->t('Description')],
      ['data' => $this->t('Provider')],
    ];

    $rows = [];
    foreach ($this->filterManager->getDefinitions() as $validation) {
      $rows[] = [
        $validation['id'],
        $validation['label'] ?? '',
        $validation['description'] ?? '',
        $validation['provider'],
      ];
    }

    return [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];
  }

}
