<?php

namespace Drupal\fapi_validation;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides dynamic permissions for fapi validation module.
 */
class FapiValidationPermissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * Returns the plugin.manager.fapi_validation_validators service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $validationManager;

  /**
   * Constructs a new FapiValidationPermissions instance.
   *
   * @param \Drupal\fapi_validation\FapiValidationValidatorsManager $validation_manager
   *   A plugin manager for Fapi Validation Validators Plugin.
   */
  public function __construct(FapiValidationValidatorsManager $validation_manager) {
    $this->validationManager = $validation_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.fapi_validation_validators')
    );
  }

  /**
   * Returns an array of fapi validation permissions.
   *
   * @return array
   *   An array of permissions for all plugins.
   */
  public function permissions() {
    $permissions = [];

    foreach ($this->validationManager->getDefinitions() as $validation) {
      $permissions["bypass {$validation['id']} fapi validation"] = [
        'title' => $this->t('Allow users to bypass @plugin validation', ['@plugin' => $validation['id']]),
      ];
    }

    return $permissions;
  }

}
