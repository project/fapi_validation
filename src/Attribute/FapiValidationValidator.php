<?php

namespace Drupal\fapi_validation\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines a FAPI Validation Validator attribute for plugin discovery.
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class FapiValidationValidator extends Plugin {

  /**
   * Constructs a FapiValidationValidator attribute.
   *
   * @param string $id
   *   The plugin ID.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|null $label
   *   (optional) The human-readable name of the validator type.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|null $description
   *   (optional) A short description of the validator type.
   * @param string|null $error_message
   *   (optional) The error message.
   * @param string|null $error_callback
   *   (optional) The callback for error messages.
   * @param class-string|null $deriver
   *   (optional) The deriver class.
   */
  public function __construct(
    public readonly string $id,
    public readonly ?TranslatableMarkup $label = NULL,
    public readonly ?TranslatableMarkup $description = NULL,
    public readonly ?string $error_message = NULL,
    public readonly ?string $error_callback = NULL,
    public readonly ?string $deriver = NULL,
  ) {}

}
