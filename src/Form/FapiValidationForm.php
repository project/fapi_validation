<?php

namespace Drupal\fapi_validation\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form handler for adding fapi validation settings.
 */
class FapiValidationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fapi_validation_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'fapi_validation.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('fapi_validation.settings');

    $form['bypass'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Bypass validations'),
      '#description' => $this->t('Allows a user to bypass Form API validations by having the required permission.'),
      '#default_value' => $config->get('bypass') ?? FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable('fapi_validation.settings')
      ->set('bypass', (bool) $form_state->getValue('bypass'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
