<?php

namespace Drupal\fapi_validation;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\fapi_validation\Exception\ValidatorException;

/**
 * A plugin manager for Fapi Validation Validators Plugin.
 */
class FapiValidationValidatorsManager extends DefaultPluginManager {

  use StringTranslationTrait;

  /**
   * Constructs a MessageManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler,
    protected readonly ConfigFactoryInterface $configFactory,
    protected readonly AccountInterface $currentUser
  ) {
    $this->alterInfo('fapi_validation_validators_info');
    $this->setCacheBackend($cache_backend, 'fapi_validation_validators');

    parent::__construct(
      'Plugin/FapiValidationValidator',
      $namespaces,
      $module_handler,
      'Drupal\fapi_validation\FapiValidationValidatorsInterface',
      'Drupal\fapi_validation\Attribute\FapiValidationValidator',
      'Drupal\fapi_validation\Annotation\FapiValidationValidator'
    );
  }

  /**
   * Check if Validator Plugin exists.
   *
   * @param string $id
   *   Validators Name.
   *
   * @return bool
   *   Check.
   */
  public function hasValidator($id) {
    return in_array($id, array_keys($this->getDefinitions()));
  }

  /**
   * Execute validation.
   *
   * @param array &$element
   *   Form Element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form State Object.
   *
   * @throws \Drupal\fapi_validation\Exception\ValidatorException
   */
  public function validate(array &$element, FormStateInterface $form_state) {
    // If the bypass config is true and the user has appropriate permissions, bypass validation.
    $config = $this->configFactory->getEditable('fapi_validation.settings');
    $bypass_config = $config->get('bypass');
    $current_user = $this->currentUser;
    if ($bypass_config && $current_user->hasPermission('bypass fapi validations')) {
      return;
    }

    // If element is empty and not required, bypass rule validation.
    $value = !isset($element['#value']) || trim($element['#value']) === '';
    if (!$element['#required'] && $value) {
      return;
    }

    $def = $element['#validators'];

    foreach ($def as $raw_validation) {
      // Parse Validator.
      $validator = new Validator($raw_validation, $form_state->getValue($element['#parents']));

      if (!$this->hasValidator($validator->getName())) {
        throw new ValidatorException("Invalid validator name '{$validator->getName()}'.");
      }

      if ($bypass_config && $current_user->hasPermission("bypass {$validator->getName()} fapi validation")) {
        return;
      }

      $plugin = $this->getDefinition($validator->getName());
      $instance = $this->createInstance($plugin['id']);

      if (!$instance->validate($validator, $element, $form_state)) {
        $error_message = $this->processErrorMessage($validator, $plugin, $element);
        $form_state->setError($element, $error_message);
      }
    }
  }

  /**
   * Process Error Message.
   *
   * @param \Drupal\fapi_validation\Validator $validator
   *   Validator.
   * @param array $plugin
   *   Plugin data.
   * @param array $element
   *   Form Element.
   *
   * @return string
   *   Error message.
   */
  protected function processErrorMessage(Validator $validator, array $plugin, array $element) {
    // User defined error callback?
    if ($validator->hasErrorCallbackDefined()) {
      return call_user_func_array($validator->getErrorCallback(), [$validator, $element]);
    }
    // User defined error message?
    elseif ($validator->hasErrorMessageDefined()) {
      $message = $validator->getErrorMessage();
    }
    // Plugin defined error callback?
    elseif (!empty($plugin['error_callback'])) {
      return call_user_func_array([$plugin['class'], $plugin['error_callback']], [$validator, $element]);
    }
    // Plugin defined error message?
    elseif (!empty($plugin['error_message'])) {
      $message = $plugin['error_message'];
    }
    else {
      $message = "Unespecified validator error message for field %field.";
    }

    return $this->t($message, ['%field' => $element['#title']]);
  }

}
