<?php

namespace Drupal\fapi_validation\Exception;

/**
 * Defines an exception thrown when the validator is not found.
 *
 * @package Drupal\fapi_validation\Exception
 */
class ValidatorException extends \Exception {}
