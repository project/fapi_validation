<?php

namespace Drupal\Tests\fapi_validation\Unit\Filters;

use Drupal\fapi_validation\Plugin\FapiValidationFilter\StripTagsFilter;
use Drupal\Tests\UnitTestCase;

/**
 * Tests filter StripTagsFilter.
 *
 * @group fapi_validation
 * @group fapi_validation_filters
 */
class StripTagsFilterTest extends UnitTestCase {

  /**
   * Testing lowercasing string.
   */
  public function testValidString() {
    $plugin = new StripTagsFilter();

    $this->assertEquals('test', $plugin->filter('<strong>test</strong>'));
    $this->assertEquals('test', $plugin->filter('test'));
  }

}
