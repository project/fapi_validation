<?php

namespace Drupal\Tests\fapi_validation\Unit\Filters;

use Drupal\fapi_validation\Plugin\FapiValidationFilter\UcfirstFilter;
use Drupal\Tests\UnitTestCase;

/**
 * Tests filter UcfirstFilterTest.
 *
 * @group fapi_validation
 * @group fapi_validation_filters
 */
class UcfirstFilterTest extends UnitTestCase {

  /**
   * Testing UcfirstFilter plugin.
   */
  public function testValidString() {
    $plugin = new UcfirstFilter();
    $this->assertEquals('Test test test', $plugin->filter('test test test'));
  }

}
