<?php

namespace Drupal\Tests\fapi_validation\Unit\Filters;

use Drupal\fapi_validation\Plugin\FapiValidationFilter\LtrimFilter;
use Drupal\Tests\UnitTestCase;

/**
 * Tests filter LtrimFilter.
 *
 * @group fapi_validation
 * @group fapi_validation_filters
 */
class LtrimFilterTest extends UnitTestCase {

  /**
   * Testing valid string.
   */
  public function testValidString() {
    $plugin = new LtrimFilter();
    $this->assertEquals('test    ', $plugin->filter('   test    '));
  }

}
