<?php

namespace Drupal\Tests\fapi_validation\Unit\Filters;

use Drupal\fapi_validation\FapiValidationFiltersManager;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests filter MachineNameFilterTest.
 *
 * @group fapi_validation
 * @group fapi_validation_filters
 */
class MachineNameFilterTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['fapi_validation'];

  /**
   * The filter plugin manager.
   *
   * @var \Drupal\fapi_validation\FapiValidationFiltersManager
   */
  protected FapiValidationFiltersManager $fapiValidationFiltersManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->fapiValidationFiltersManager = $this->container->get('plugin.manager.fapi_validation_filters');
  }

  /**
   * Testing machine_name`s filter plugin.
   */
  public function testValidString() : void {
    $plugin = $this->fapiValidationFiltersManager->createInstance('machine_name');
    $this->assertEquals('test_test', $plugin->filter('TesT test'));
  }

}
