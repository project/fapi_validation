<?php

namespace Drupal\Tests\fapi_validation\Unit\Filters;

use Drupal\fapi_validation\Plugin\FapiValidationFilter\UcwordsFilter;
use Drupal\Tests\UnitTestCase;

/**
 * Tests filter UcwordsFilterTest.
 *
 * @group fapi_validation
 * @group fapi_validation_filters
 */
class UcwordsFilterTest extends UnitTestCase {

  /**
   * Testing UcwordsFilter filter.
   */
  public function testValidString() {
    $plugin = new UcwordsFilter();
    $this->assertEquals('Test Test Test', $plugin->filter('test test test'));
  }

}
