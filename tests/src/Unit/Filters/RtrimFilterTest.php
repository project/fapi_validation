<?php

namespace Drupal\Tests\fapi_validation\Unit\Filters;

use Drupal\fapi_validation\Plugin\FapiValidationFilter\RtrimFilter;
use Drupal\Tests\UnitTestCase;

/**
 * Tests filter RtrimFilter.
 *
 * @group fapi_validation
 * @group fapi_validation_filters
 */
class RtrimFilterTest extends UnitTestCase {

  /**
   * Testing valid string.
   */
  public function testValidString() {
    $plugin = new RtrimFilter();
    $this->assertEquals('   test', $plugin->filter('   test    '));
  }

}
