<?php

namespace Drupal\Tests\fapi_validation\Unit\Validators;

use Drupal\fapi_validation\Plugin\FapiValidationValidator\NumericValidator;
use Drupal\fapi_validation\Validator;

/**
 * Tests numeric validator.
 *
 * @group fapi_validation
 * @group fapi_validation_validators
 */
class NumericValidatorTest extends BaseValidator {

  /**
   * Numeric validation.
   *
   * @var \Drupal\fapi_validation\FapiValidationValidatorsInterface
   */
  protected $plugin;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->plugin = new NumericValidator();
  }

  /**
   * Testing valid string.
   */
  public function testValidString() {
    $validator = new Validator('numeric', '675745634452');

    $this->assertTrue($this->plugin->validate($validator, [], $this->decoratedFormState));
  }

  /**
   * Testing invalid string.
   */
  public function testInvalidString() {
    $validator = new Validator('numeric', 'sfs343dgfs2dfs');

    $this->assertFalse($this->plugin->validate($validator, [], $this->decoratedFormState));
  }

}
