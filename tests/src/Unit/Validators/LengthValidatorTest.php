<?php

namespace Drupal\Tests\fapi_validation\Unit\Validators;

use Drupal\fapi_validation\Plugin\FapiValidationValidator\LengthValidator;
use Drupal\fapi_validation\Validator;

/**
 * Tests Length Validator.
 *
 * @group fapi_validation
 * @group fapi_validation_validators
 */
class LengthValidatorTest extends BaseValidator {

  /**
   * Alpha validation.
   *
   * @var \Drupal\fapi_validation\FapiValidationValidatorsInterface
   */
  protected $plugin;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->plugin = new LengthValidator();
  }

  /**
   * Testing valid string.
   */
  public function testValidString() {
    $validator = new Validator('length[5, *]', 'dljshfkjshf');

    $this->assertTrue($this->plugin->validate($validator, [], $this->decoratedFormState));
  }

  /**
   * Testing invalid string.
   */
  public function testInvalidString() {
    $validator = new Validator('length[5, *]', 'gwe');

    $this->assertFalse($this->plugin->validate($validator, [], $this->decoratedFormState));
  }

}
