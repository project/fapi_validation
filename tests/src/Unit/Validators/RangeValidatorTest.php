<?php

namespace Drupal\Tests\fapi_validation\Unit\Validators;

use Drupal\fapi_validation\Plugin\FapiValidationValidator\RangeValidator;
use Drupal\fapi_validation\Validator;

/**
 * Tests Range Validator.
 *
 * @group fapi_validation
 * @group fapi_validation_validators
 */
class RangeValidatorTest extends BaseValidator {

  /**
   * Numeric validation.
   *
   * @var \Drupal\fapi_validation\FapiValidationValidatorsInterface
   */
  protected $plugin;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->plugin = new RangeValidator();
  }

  /**
   * Testing valid string.
   */
  public function testValidString() {
    $validator = new Validator('range[5, 100]', '95');

    $this->assertTrue($this->plugin->validate($validator, [], $this->decoratedFormState));
  }

  /**
   * Testing invalid string.
   */
  public function testInvalidString() {
    $validator = new Validator('range[5, 10]', '3');

    $this->assertFalse($this->plugin->validate($validator, [], $this->decoratedFormState));
  }

}
